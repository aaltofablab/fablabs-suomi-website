+++

title = "What is this Webpage for?"

+++

The purpose for the web page is to share general knowledge about Fab Labs but especially deepen the knowledge of active Finnish Fab Labs, their facilities and different events arranged in them. We want to showcase projects and tutorials made in Finnish fab labs, which you are free to use and adapt for your own projects.

The aim is also to provide information to those interested in joining the Fab Lab network. On the Fab Foundation website, you will find lots of helpful information to get you started in setting up a Fab Lab. You can also contact the Finnish Fab Labs directly through the website and ask for help.