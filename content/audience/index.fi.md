+++

title = "Mistä verkkosivussa on kyse?"

+++

Verkkosivun tarkoituksena on jakaa yleistä tietoa Fab Labeista, mutta erityisesti syventää tietämystä Suomen aktiivisista Fab Labeista, niiden tiloista ja niissä järjestettävistä erilaisista tapahtumista. Haluamme esitellä suomalaisissa Fab Labsissa tehtyjä projekteja ja tutorialeja, joita voit vapaasti käyttää ja muokata omiin projekteihisi sopiviksi.

Tavoitteena on myös tarjota tietoa Fab Lab -verkostoon liittymisestä kiinnostuneille. Fab Foundationin verkkosivuilta löydät paljon hyödyllistä tietoa, jonka avulla pääset alkuun Fab Labin perustamisessa. Voit myös ottaa yhteyttä suoraan suomalaisiin Fab Labeihin verkkosivujen kautta ja pyytää apua. 
