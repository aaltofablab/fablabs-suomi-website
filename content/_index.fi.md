+++

title = "Tervetuloa!"

+++

Suomalainen fab lab -verkosto on yksilöiden ja organisaatioiden yhteisö, joka on omistautunut teknologian ymmärtämiseen ja siihen, että ihmiset voivat ymmärtää ja muokata heitä ympäröivää teknologiaa. Tavoitteenamme on tarjota pääsy työkaluihin ja resursseihin, joita tarvitaan erilaisten tuotteiden suunnitteluun, prototyyppien luomiseen ja niiden taustalla olevan teknologian ymmärtämiseen.

