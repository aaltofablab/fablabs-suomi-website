+++
[[meetings]]
title = "Kuukausikokous"
host = "Aalto Fablab"
date = "2023-05-23"
time = "15:00"

[[meetings]]
title = "Kuukausikokous"
host = "Fab Lab Oulu"
date = "2023-08-15"
time = "15:00"

[[meetings]]
title = "Fablab Suomi Bootcamp"
host = "Fab Lab Oulu"
date = "2023-08-30"
time = "09:00"
+++