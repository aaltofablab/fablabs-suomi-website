+++
title = "Verkkoon"
+++

Eri puolilla maata sijaitsevien fab labien verkostomme kautta tarjoamme erilaisia palveluja, kuten koulutusta teknologiasta ja digitaalisista valmistustekniikoista, uusimpien laitteiden ja tekniikan käyttöä sekä mahdollisuuksia yhteistyöhön ja verkostoitumiseen muiden tekijöiden ja innovaattoreiden kanssa. Järjestämme myös tapahtumia, työpajoja ja hackathoneja, joilla edistämme alan tiedon ja asiantuntemuksen jakamista ja autamme ihmisiä ymmärtämään, miten teknologia toimii ja miten sitä voidaan käyttää heidän hyödykseen.

Olitpa sitten opiskelija, ammattilainen tai harrastaja, Suomen fab lab -verkosto toivottaa tervetulleeksi kaikki, jotka ovat kiinnostuneita teknologiasta ja ympäröivän teknologian muokkaamisesta. Liity jo tänään ja tule osaksi kasvavaa yhteisöä, joka koostuu ihmisistä, jotka haluavat ymmärtää ja muokata tulevaisuuden teknologiaa!



