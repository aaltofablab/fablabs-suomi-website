+++

title = "Welcome!"

+++

The Finnish Fab Lab Network is a community of individuals and organizations dedicated to demystifying technology and empowering people to understand and shape the technology surrounding them. Our goal is to provide access to the tools and resources needed to design, prototype and understand the underlying technology of a wide range of products.

